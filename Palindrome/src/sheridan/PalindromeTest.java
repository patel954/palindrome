package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome( "anna" ));
	}
	
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome( "Anna has a lamb" ));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn () {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("Aa"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut () {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("racer car"));
	}
}